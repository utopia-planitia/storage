#!/bin/bash
set -euxo pipefail

CHART_VERSION=$(yq -r '.releases[] | select( .chart == "rook-release/rook-ceph" ) | .version' rook/helmfile.yaml | sort | uniq)

echo "CHART_VERSION: ${CHART_VERSION}"

curl -fsSL -o ceph/src/toolbox.yaml https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/toolbox.yaml

curl -fsSL -o ceph/src/cluster.yaml https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/cluster.yaml

rm -f ceph/chart/templates/cephcluster.yaml
rm -f ceph/chart/templates/deployment-rook-ceph-tools.yaml

kustomize build ceph/src \
    | chart-prettier ceph/chart/templates

curl -fsSL -o ceph-init/src/rbd-storageclass.yaml https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/csi/rbd/storageclass.yaml

curl -fsSL -o ceph-init/src/cepfs-filesystem.yaml https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/filesystem.yaml
curl -fsSL -o ceph-init/src/cepfs-storageclass.yaml https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/csi/cephfs/storageclass.yaml

kustomize build ceph-init/src \
    | chart-prettier --truncate ceph-init/chart/templates

cp ceph-init/chart/templates/storageclass-rook-ceph-block.yaml ceph-init/chart/templates/storageclass-rook-ceph-block-xfs.yaml
sed -i 's/rook-ceph-block/rook-ceph-block-xfs/g' ceph-init/chart/templates/storageclass-rook-ceph-block-xfs.yaml
sed -i 's/ext4/xfs/g' ceph-init/chart/templates/storageclass-rook-ceph-block-xfs.yaml
cat ceph-init/chart/templates/storageclass-rook-ceph-block.yaml \
    | grep -v annotations \
    | grep -v is-default-class \
    > ceph-init/chart/templates/storageclass-rook-ceph-block.yaml_
mv ceph-init/chart/templates/storageclass-rook-ceph-block.yaml_ ceph-init/chart/templates/storageclass-rook-ceph-block.yaml

curl -fsSL https://raw.githubusercontent.com/rook/rook/${CHART_VERSION}/deploy/examples/crds.yaml \
    | chart-prettier --truncate rook-crds/chart/templates
