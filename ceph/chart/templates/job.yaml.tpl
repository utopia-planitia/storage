{{ if .Release.IsInstall }}
apiVersion: batch/v1
kind: Job
metadata:
  name: set-dashboard-password
  namespace: rook-ceph
spec:
  backoffLimit: 20
  template:
    metadata:
      name: set-dashboard-password
    spec:
      dnsPolicy: ClusterFirstWithHostNet
      containers:
        - name: rook-ceph-tools
          image: docker.io/rook/ceph:v1.16.5@sha256:097fa457e23ee42b99ac6ebf9eedd4043d7eb6de7f0ffd142455158c395c39d0
          command:
            - sh
            - -euxc
          args:
            - /usr/local/bin/toolbox.sh & sleep 5 && ceph dashboard ac-user-set-password admin -i /dashboard/password
          imagePullPolicy: IfNotPresent
          env:
            - name: ROOK_CEPH_USERNAME
              valueFrom:
                secretKeyRef:
                  name: rook-ceph-mon
                  key: ceph-username
            - name: ROOK_CEPH_SECRET
              valueFrom:
                secretKeyRef:
                  name: rook-ceph-mon
                  key: ceph-secret
          volumeMounts:
            - mountPath: /etc/ceph
              name: ceph-config
            - name: mon-endpoint-volume
              mountPath: /etc/rook
            - name: rook-ceph-dashboard-password
              mountPath: /dashboard
      volumes:
        - name: mon-endpoint-volume
          configMap:
            name: rook-ceph-mon-endpoints
            items:
              - key: data
                path: mon-endpoints
        - name: rook-ceph-dashboard-password
          secret:
            secretName: rook-ceph-dashboard-password
        - name: ceph-config
          emptyDir: {}
      tolerations:
        - key: "node.kubernetes.io/unreachable"
          operator: "Exists"
          effect: "NoExecute"
          tolerationSeconds: 5
      restartPolicy: OnFailure
{{ end }}
