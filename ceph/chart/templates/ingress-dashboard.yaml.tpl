apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    kubernetes.io/ingress.class: nginx
  name: dashboard
spec:
  rules:
    - host: ceph.{{ .Values.cluster.domain }}
      http:
        paths:
          - backend:
              service:
                name: rook-ceph-mgr-dashboard
                port:
                  number: 8080
            path: /
            pathType: ImplementationSpecific
  tls:
    - hosts:
        - ceph.{{ .Values.cluster.domain }}
      secretName: "{{ .Values.cluster.tls_wildcard_secret }}"
