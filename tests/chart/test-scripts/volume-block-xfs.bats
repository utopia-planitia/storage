#!/usr/bin/env bats

cleanup() {
  kubectl --namespace=rook-ceph delete \
    --filename=volume-claim-block-xfs.yml \
    --filename=volume-read-block-xfs.yml \
    --filename=volume-write-block-xfs.yml \
    --ignore-not-found=true
}

setup() {
  cleanup
}

teardown() {
  # BATS_TEST_COMPLETED which will be set to 1 if the test was successful
  # https://bats-core.readthedocs.io/en/stable/faq.html#how-can-i-check-if-a-test-failed-succeeded-during-teardown
  if test "${BATS_TEST_COMPLETED:-0}" -eq 1; then cleanup; fi
}

@test "create, write to and read from persistent volumes" {
  run kubectl --namespace=rook-ceph apply --filename=volume-claim-block-xfs.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl --namespace=rook-ceph wait --filename=volume-claim-block-xfs.yml --for=jsonpath='{.status.phase}'=Bound --timeout=60s
  if test "$status" -ne 0; then
    kubectl --namespace=rook-ceph describe --filename=volume-claim-block-xfs.yml >&2
  fi
  [ $status -eq 0 ]

  run kubectl --namespace=rook-ceph apply --filename=volume-write-block-xfs.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl --namespace=rook-ceph wait --filename=volume-write-block-xfs.yml --for=condition=complete --timeout=60s
  if test "$status" -ne 0; then
    kubectl --namespace=rook-ceph describe --filename=volume-write-block-xfs.yml >&2
  fi
  [ $status -eq 0 ]

  run kubectl --namespace=rook-ceph apply --filename=volume-read-block-xfs.yml
  [ $status -eq 0 ]
  sleep 1

  run kubectl --namespace=rook-ceph wait --filename=volume-read-block-xfs.yml --for=condition=complete --timeout=60s
  if test "$status" -ne 0; then
    kubectl --namespace=rook-ceph describe --filename=volume-read-block-xfs.yml >&2
  fi
  [ $status -eq 0 ]
}
