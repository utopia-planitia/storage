#/bin/bash
set -euxo pipefail

# setup
mkdir /workbench
cd /workbench

# connect to minio
mc alias set rook-minio https://{{ .Values.minio_public_domain }} {{ .Values.minio_access_key }} {{ .Values.minio_secret_key }}

# tear down previous test artifacts if there are any
mc rb --force rook-minio/test-public-minio || true

# create bucket
mc mb rook-minio/test-public-minio
[ "$(mc ls rook-minio | grep test-public-minio | wc -l)" -eq 1 ]

# assert that `mc mb --ignore-existing` does not fail
mc mb --ignore-existing rook-minio/test-public-minio

# test if “mc mb” can be used to create a directory
# from the docs: “The mc mb command creates a new bucket or directory at the specified path.”
# <https://min.io/docs/minio/linux/reference/minio-mc/mc-mb.html>
mc mb --ignore-existing rook-minio/test-public-minio/empty-directory/
mc stat rook-minio/test-public-minio/empty-directory/ >/dev/null
mc rm rook-minio/test-public-minio/empty-directory/

# file transfer
echo hello > transfer-file
mc cp transfer-file rook-minio/test-public-minio
mc cp rook-minio/test-public-minio/transfer-file transfer-file-returned
diff transfer-file transfer-file-returned

# cleanup file
mc rm rook-minio/test-public-minio/transfer-file
[ -z "$(mc ls rook-minio/test-public-minio)" ]

# cleanup bucket
mc rb rook-minio/test-public-minio
[ "$(mc ls rook-minio | grep test-public-minio -c || true)" -eq 0 ]
