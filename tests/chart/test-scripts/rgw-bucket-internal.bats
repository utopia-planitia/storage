#!/usr/bin/env bats

cleanup() {
  kubectl delete -f rgw-bucket-internal-claim.yaml -f rgw-access-internal.yaml --ignore-not-found=true
}

setup() {
  cleanup
}

teardown() {
  kubectl -n rook-ceph logs job/rgw-access-internal

  cleanup

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "test internal rgw" {
  run kubectl apply -f rgw-bucket-internal-claim.yaml -f rgw-access-internal.yaml
  [ $status -eq 0 ]
  sleep 1

  run kubectl -n rook-ceph wait --for=condition=complete --timeout=60s job/rgw-access-internal
  [ $status -eq 0 ]
}
