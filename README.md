# Dashboard

A deployment of Rook https://github.com/rook/rook

Use Rook to manage Ceph as storage https://rook.io/docs/rook/master/client.html

## Usage

run `make` for usage

## Access

`kubectl -n rook exec -it rook-tools bash`

```
rookctl status
ceph df
rados df
```

## Minio

```sh
# configure an alias
kubectl --namespace rook-ceph exec pod/minio-0 -- sh -euc 'mc alias set minio http://minio-headless:9000 "${MINIO_ROOT_USER}" "${MINIO_ROOT_PASSWORD}"'
# display the MinIO server information
kubectl --namespace rook-ceph exec pod/minio-0 -- mc admin info minio
```
